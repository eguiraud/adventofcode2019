def check(n):
    s = str(n)
    has_double = False
    for i in range(len(s) - 1):
        if s[i] == s[i+1]:
            has_double = True
        if int(s[i]) > int(s[i + 1]):
            return False
    if not has_double:
        return False
    return True

def check2(n):
    s = str(n)
    has_double = False
    in_group = -1
    good_group = False
    for i in range(len(s) - 1):
        if s[i] == s[i+1]:
            if int(s[i]) == in_group:
                good_group = False
            else:
                good_group = True
                in_group = int(s[i])
        elif good_group:
            has_double = True
            in_group = -1
        if int(s[i]) > int(s[i + 1]):
            return False
    if not has_double and not good_group:
        return False
    return True

if __name__ == "__main__":
    puzzle_input = "264793-803935"
    start, finish = map(int, puzzle_input.split("-"))
    counter = 0
    for n in range(start, finish + 1):
        counter += check(n)
    print(counter)
    counter = 0
    for n in range(start, finish + 1):
        counter += check2(n)
    print(counter)
