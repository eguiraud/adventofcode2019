from typing import List, Tuple


def _demangle_opcode(opcode: int) -> Tuple[int, Tuple[int, ...]]:
    op = opcode % 100
    par_modes = tuple(opcode % (div * 10) // div for div in (100, 1000, 10000))
    return op, par_modes


def _arg(p, idx, par_mode):
    return p[p[idx]] if par_mode == 0 else p[idx]


def compute(program: List[int], inputs: List[int] = None) -> List[int]:
    p = program.copy()  # don't want to overwrite input
    ip = 0  # instruction pointer
    opcode, par_modes = _demangle_opcode(p[ip])
    outputs = []  # list of things printed to stdout
    while opcode != 99:
        ip += 1

        if opcode in (1, 2):
            # sum or multiplication
            assert par_modes[2] == 0
            op = {1: int.__add__, 2: int.__mul__}[opcode]
            p[p[ip + 2]] = op(_arg(p, ip, par_modes[0]), _arg(p, ip + 1, par_modes[1]))
            nargs = 3
        elif opcode == 3:
            # read
            assert inputs is not None and len(inputs) > 0
            n = inputs.pop(0)  # read one input
            p[p[ip]] = n
            nargs = 1
        elif opcode == 4:
            # write
            outputs.append(_arg(p, ip, par_modes[0]))
            nargs = 1
        elif opcode in (5, 6):
            # jump ge 0, jump eq 0
            b = _arg(p, ip, par_modes[0])
            if b > 0 and opcode == 5 or b == 0 and opcode == 6:
                ip = _arg(p, ip + 1, par_modes[1])
                nargs = 0
            else:
                nargs = 2
        elif opcode in (7, 8):
            # le, eq
            assert par_modes[2] == 0
            a, b = (_arg(p, ip + idx, par_modes[idx]) for idx in range(2))
            cond = a < b if opcode == 7 else a == b
            p[p[ip + 2]] = 1 if cond else 0
            nargs = 3
        else:
            raise ValueError(f"Unknown opcode: {opcode}")

        ip += nargs
        opcode, par_modes = _demangle_opcode(p[ip])
    return p, outputs

