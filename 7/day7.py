import sys
sys.path.append("../")
from intcode_computer import compute
from itertools import permutations
import numpy as np
import math
from typing import List


def thrusters_output(program: List[int], phases: List[int]) -> int:
    assert len(phases) == 5, "we have 5 thruster amplifiers!"
    a_input = 0
    for a in range(5):
        p, (a_output,) = compute(program, inputs=[phases[a], a_input])
        a_input = a_output
    return a_output


def test():
    program = list(map(int, "3,15,3,16,1002,16,10,16,1,16,15,15,4,15,99,0,0".split(",")))
    max_output, max_phase = max((thrusters_output(program, phase), phase) for phase in permutations(range(5), 5))
    assert max_output == 43210
    assert max_phase == (4,3,2,1,0)


if __name__ == "__main__":
    test()

    program = np.loadtxt("input.txt", delimiter=',', dtype=np.int32).tolist()
    max_output = max((thrusters_output(program, phase), phase) for phase in permutations(range(5), 5))[0]
    print(max_output)
