import numpy as np
from typing import List
import sys
from itertools import product

def compute(program: List[int]) -> List[int]:
    p = program.copy()  # don't want to overwrite input

    pos = 0
    opcode = p[pos]
    while opcode != 99:
        par1 = p[pos + 1]
        par2 = p[pos + 2]
        out = p[pos + 3]
        op = {1: int.__add__, 2: int.__mul__}[opcode]
        p[out] = op(p[par1], p[par2])
        pos += 4
        opcode = p[pos]

    return p

def test() -> None:
    programs = [[1,0,0,0,99], [2,3,0,3,99], [2,4,4,5,99,0], [1,1,1,4,99,5,6,0,99]]
    outputs = [[2,0,0,0,99], [2,3,0,6,99], [2,4,4,5,99,9801], [30,1,1,4,2,5,6,0,99]]
    for p, o in zip(programs, outputs):
        r = compute(p)
        assert r == o, f"{p} yields {r} instead of {o}"

if __name__ == "__main__":
    if len(sys.argv) > 1 and sys.argv[1] == "--test":
        test()

    program = np.loadtxt("input.txt", delimiter=',', dtype=np.int32).tolist()

    # part 1
    p = program.copy()
    p[1] = 12
    p[2] = 2
    output = compute(p)
    print(output[0])

    # part 2
    for noun, verb in product(range(100), range(100)):
        p = program.copy()
        p[1] = noun
        p[2] = verb
        output = compute(p)
        if output[0] == 19690720:
            break
    print(noun, verb)
