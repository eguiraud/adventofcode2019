def parse_orbits(f):
    orbits = {}
    for l in f:
        A, B = l.strip().split(")")
        orbits[B] = A
    return dict(orbits)

def count_orbits(orbits, child=None):
    if child is None:
        n = 0
        for child in orbits:
            n += 1 + count_orbits(orbits, child=orbits[child])
        return n
    elif child == "COM":
        return 0
    else:
        return 1 + count_orbits(orbits, child=orbits[child])

def test1():
    with open("test1.txt") as test_f:
        orbits = parse_orbits(test_f)
    assert count_orbits(orbits) == 42

def parent_list(orbits, A):
    if A == "COM":
        return []
    else:
        parent = orbits[A]
        return [parent] + parent_list(orbits, parent)

def common_parent(orbits, A, B):
    Aparents = parent_list(orbits, A)
    Bparent = B
    while Bparent not in Aparents:
        Bparent = orbits[Bparent]
    return Bparent

def distance(orbits, A, B):
    d = 0
    p = orbits[A]
    if orbits[A] == orbits[B]:
        return 0
    while p != B:
        if p == "COM":
            raise ValueError(f"{B} unreachable from {A}")
        p = orbits[p]
        d += 1
    return d

def test2():
    with open("test2.txt") as test_f:
        orbits = parse_orbits(test_f)
    p = common_parent(orbits, "YOU", "SAN")
    assert distance(orbits, "YOU", p) + distance(orbits, "SAN", p) == 4

if __name__ == "__main__":
    # part 1
    test1()
    with open("input.txt") as test_f:
        orbits = parse_orbits(test_f)
    print(count_orbits(orbits))

    # part 2
    test2()
    with open("input.txt") as test_f:
        p = common_parent(orbits, "YOU", "SAN")
        print(distance(orbits, "YOU", p) + distance(orbits, "SAN", p))
