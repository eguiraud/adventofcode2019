import numpy as np
from typing import List, Callable, Tuple
import sys
from itertools import product

def demangle_opcode(opcode: int) -> Tuple[Callable, int]:
    op = opcode % 100
    par_modes = tuple(opcode % (div * 10) // div for div in (100, 1000, 10000))
    return op, par_modes

def arg(p, idx, par_mode):
    return p[p[idx]] if par_mode == 0 else p[idx]

def compute(program: List[int], inputs: List[int] = None) -> None:
    p = program.copy()  # don't want to overwrite input
    ip = 0  # instruction pointer
    opcode, par_modes = demangle_opcode(p[ip])
    outputs = []  # list of things printed to stdout
    while opcode != 99:
        ip += 1

        if opcode in (1, 2):
            assert par_modes[2] == 0
            op = {1: int.__add__, 2: int.__mul__}[opcode]
            p[p[ip + 2]] = op(arg(p, ip, par_modes[0]), arg(p, ip + 1, par_modes[1]))
            nargs = 3
        elif opcode == 3:
            n = inputs.pop(0)  # read one input
            p[p[ip]] = n
            nargs = 1
        elif opcode == 4:
            outputs.append(arg(p, ip, par_modes[0]))
            nargs = 1
        elif opcode in (5, 6):
            b = arg(p, ip, par_modes[0])
            if b > 0 and opcode == 5 or b == 0 and opcode == 6:
                ip = arg(p, ip + 1, par_modes[1])
                nargs = 0
            else:
                nargs = 2
        elif opcode in (7, 8):
            assert par_modes[2] == 0
            a, b = (arg(p, ip + idx, par_modes[idx]) for idx in range(2))
            cond = a < b if opcode == 7 else a == b
            p[p[ip + 2]] = 1 if cond else 0
            nargs = 3
        else:
            raise ValueError(f"Unknown opcode: {opcode}")

        ip += nargs
        opcode, par_modes = demangle_opcode(p[ip])
    return p, outputs

def test() -> None:
    programs = [[1,0,0,0,99], [2,3,0,3,99], [2,4,4,5,99,0], [1,1,1,4,99,5,6,0,99]]
    outputs = [[2,0,0,0,99], [2,3,0,6,99], [2,4,4,5,99,9801], [30,1,1,4,2,5,6,0,99]]
    for p, o in zip(programs, outputs):
        r, _ = compute(p)
        assert r == o, f"{p} yields {r} instead of {o}"

    test_inout = [3,0,4,0,99]
    _, out = compute(test_inout, inputs=[42])
    assert out == [42]

    test_parmodes = [1002,4,3,4,33]
    r, _ = compute(test_parmodes)
    assert r == [1002,4,3,4,99]

if __name__ == "__main__":
    test()

    program = np.loadtxt("input.txt", delimiter=',', dtype=np.int32).tolist()

    # part 1
    _, outs = compute(program, inputs=[1])
    print(outs)  # should print [0, 0, 0, 0, 0, 0, 0, 0, 0, 5182797]

    # part 2
    _, outs = compute(program, inputs=[5])
    print(outs)
