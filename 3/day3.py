from itertools import product
from typing import List, Tuple, Set

Point = Tuple[int, int]

def get_points(wire_data: List[str]) -> List[Point]:
    """Convert wire data to list of datapoints.

    Wire data is a list of strings of form (R|L|U|D)[0-9]+.
    Returned datapoints are a set of pairs of integers.
    """
    moves = {"R": lambda x, y: (x + 1, y),
             "L": lambda x, y: (x - 1, y),
             "U": lambda x, y: (x, y + 1),
             "D": lambda x, y: (x, y - 1)}
    pos = (0, 0)
    points = [pos]
    for s in wire_data:
        move = moves[s[0]]
        distance = int(s[1:])
        for _ in range(distance):
            pos = move(*pos)
            points.append(pos)
    return points


def get_intersections(wire1: List[Point], wire2: List[Point]) -> Set[Point]:
    return set(wire1).intersection(set(wire2)) - {(0,0)}  # discard intersection at (0, 0)


def min_intersection_distance(wire1: List[Point], wire2: List[Point]) -> int:
    """Return Manhattan distance from (0, 0) of the closest intersection of wire1 and wire2."""
    intersections = get_intersections(wire1, wire2)
    return min(map(lambda p: abs(p[0]) + abs(p[1]), intersections))


def min_total_length_distance(wire1: List[Point], wire2: List[Point]) -> int:
    """Total length of wires at intersection point that minimizes it."""
    intersections = get_intersections(wire1, wire2)
    def length(wire, point):
        """Number of points wire passes through to get to point."""
        l = 0
        for p in wire[1:]:
            l += 1
            if p == point:
                break
        else:
            raise ValueError(f"Wire does not pass through point {point}")
        return l
    return min(length(wire1, int_point) + length(wire2, int_point) for int_point in intersections)


def test():
    wires1 = map(get_points, [["R75","D30","R83","U83","L12","D49","R71","U7","L72"],
                              ["R98","U47","R26","D63","R33","U87","L62","D20","R33","U53","R51"]])
    wires2 = map(get_points, [["U62","R66","U55","R34","D71","R55","D58","R83"],
                              ["U98","R91","D20","R16","D67","R40","U7","R15","U6","R7"]])
    solution_part1 = (159, 135)
    solution_part2 = (610, 410)
    for w1, w2, s1, s2 in zip(wires1, wires2, solution_part1, solution_part2):
        d1 = min_intersection_distance(w1, w2)
        d2 = min_total_length_distance(w1, w2)
        assert d1 == s1, f"expected {s1}, got {d1}"
        assert d2 == s2, f"expected {s2}, got {d2}"
        assert d1 == min_intersection_distance(w2, w1), "distance 1 is not commutative"
        assert d2 == min_total_length_distance(w2, w1), "distance 2 is not commutative"


if __name__ == "__main__":
    test()

    with open("input.txt") as wire_data:
        w1, w2 = [get_points(w.strip().split(",")) for w in wire_data.readlines()]
        min_d1 = min_intersection_distance(w1, w2)
        min_d2 = min_total_length_distance(w1, w2)
        print(min_d1)
        print(min_d2)
        
