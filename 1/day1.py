import numpy as np

def fuel1(mass):
    return mass // 3 - 2

def fuel2(mass):
    if mass <= 0:
        return 0
    else:
        f = max(0, mass // 3 - 2)
        return f + fuel2(f)

if __name__ == "__main__":
    masses = np.loadtxt("input.txt", dtype=np.int32)
    part1 = sum(map(fuel1, masses))
    part2 = sum(map(fuel2, masses))
    print(f"part 1: {part1}")
    print(f"part 2: {part2}")
